package com.tdp2.utils.extensions

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.view.View
import android.view.WindowManager
import com.tdp2.utils.R

fun Activity.isCameraAvailable(): Boolean {
    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    return takePictureIntent.resolveActivity(this.packageManager) != null
}

@Suppress("unused")
fun Activity.sendEmail(email: String, subject: String = "", mainView: View) {
    val intent = Intent(Intent.ACTION_SENDTO)
    intent.data = Uri.parse("mailto:")
    intent.putExtra(Intent.EXTRA_EMAIL, email)
    intent.putExtra(Intent.EXTRA_SUBJECT, subject)

    try {
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        mainView.showSnackbar(R.string.error_email_app_not_available)
    }
}

@Suppress("unused")
fun Activity.openUrl(url: String, mainView: View) {

    var path = url
    if (!path.contains("http://", ignoreCase = true) && !path.contains(
            "https://",
            ignoreCase = true
        )
    ) {
        path = "http://$url"
    }

    try {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(path))
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        mainView.showSnackbar(R.string.error_web_app_not_available)
    }
}

@Suppress("unused")
fun Activity.call(phone: String, mainView: View) {
    val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
    startActivity(intent)

    try {
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        mainView.showSnackbar(R.string.error_phone_app_not_available)
    }
}

@Suppress("unused")
fun Activity.makeStatusBarTransparent() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
        }
    }
}
