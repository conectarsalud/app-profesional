package com.tdp2.utils.ui.openGraphView

import android.content.Context
import android.graphics.*
import android.media.ThumbnailUtils
import android.util.AttributeSet
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import com.tdp2.utils.R

class RoundableImageView : androidx.appcompat.widget.AppCompatImageView {

    private var mSide = 0
    private var mMargin = 0
    private var mCornerRadius = 0f
    private val mRect = RectF()
    private val mPaint = Paint()
    private var mPosition: OpenGraphView.IMAGE_POSITION = OpenGraphView.IMAGE_POSITION.LEFT

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        mPaint.isAntiAlias = true
    }

    fun setMargin(viewSize: Int, margin: Int) {
        mMargin = margin
        mSide = viewSize
        val params = layoutParams as RelativeLayout.LayoutParams
        params.setMargins(margin, margin, margin, margin)
        params.width = mSide
        params.height = mSide
        layoutParams = params
        invalidate()
    }

    fun setRadius(radius: Float) {
        if (mCornerRadius == radius) {
            return
        }
        mCornerRadius = radius
        invalidate()
    }

    fun setPosition(position: OpenGraphView.IMAGE_POSITION) {
        mPosition = position
        setImageParam(if (mPosition == OpenGraphView.IMAGE_POSITION.LEFT) RelativeLayout.ALIGN_PARENT_LEFT else RelativeLayout.ALIGN_PARENT_RIGHT)
        invalidate()
    }

    private fun setImageParam(rule: Int) {
        val imageParams = RelativeLayout.LayoutParams(mSide, mSide)
        imageParams.addRule(rule)
        imageParams.topMargin = mMargin
        imageParams.bottomMargin = mMargin
        layoutParams = imageParams
    }

    override fun setImageBitmap(bm: Bitmap?) {
        super.setImageBitmap(bm)
        if (bm == null) {
            mPaint.reset()
            mPaint.color = ContextCompat.getColor(context, R.color.colorTransparent)
            invalidate()
            return
        } else {
            mPaint.color = ContextCompat.getColor(context, R.color.colorWhite)
        }

        val centerCroppedBitmap = ThumbnailUtils.extractThumbnail(bm, mSide, mSide)
        val shader =
            BitmapShader(centerCroppedBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        mPaint.shader = shader
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        if (mPosition == OpenGraphView.IMAGE_POSITION.LEFT) {
            mRect[0f, 0f, mSide.toFloat()] = mSide.toFloat()
            canvas!!.drawRoundRect(mRect, mCornerRadius, mCornerRadius, mPaint)
            mRect[mSide - mCornerRadius, 0f, mSide.toFloat()] = mSide.toFloat()
            canvas.drawRect(mRect, mPaint)
        } else {
            mRect[0f, 0f, mSide.toFloat()] = mSide.toFloat()
            canvas!!.drawRoundRect(mRect, mCornerRadius, mCornerRadius, mPaint)
            mRect[0f, 0f, mCornerRadius] = mSide.toFloat()
            canvas.drawRect(mRect, mPaint)
        }
    }
}