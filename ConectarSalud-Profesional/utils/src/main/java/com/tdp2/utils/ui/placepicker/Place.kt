package com.tdp2.utils.ui.placepicker

import com.tdp2.utils.extensions.append
import com.google.android.gms.maps.model.LatLng
import java.io.Serializable

data class Place(
    var id: String? = null,
    var description: String? = null,
    var lat: Double? = null,
    var long: Double? = null,
    var address: String? = null,
    var city: String? = null,
    var state: String? = null,
    var country: String? = null,
    var zipcode: String? = null
) : Serializable {

    val addressDetail: String
        get() {
            return address.append(city, " ")
                .append(state, ", ")
                .append(country, ", ")
                .append(zipcode, ", ")
        }

    fun getCoordinates(): LatLng?{
        if(lat != null && long != null){
            return LatLng(lat!!,long!!)
        }

        return null
    }

    fun setCoordinate(coord: LatLng?){
        lat = null
        long = null
        coord?.apply {
            lat = latitude
            long = longitude
        }
    }
}