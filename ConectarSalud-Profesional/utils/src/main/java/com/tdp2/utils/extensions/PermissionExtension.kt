package com.tdp2.utils.extensions

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.tdp2.utils.R

@Suppress("unused")
fun Activity.requestPermissionForCamera(requestCode: Int) {
    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
        Toast.makeText(this, getString(R.string.permission_camera), Toast.LENGTH_SHORT).show()
    } else {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), requestCode)
    }
}

@Suppress("unused")
fun Activity.checkPermissionForCamera(): Boolean {
    return when (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)) {
        PackageManager.PERMISSION_GRANTED -> true
        else -> false
    }
}

@Suppress("unused")
fun Fragment.requestPermissionForCamera(requestCode: Int) {
    this.activity?.let {
        if (ActivityCompat.shouldShowRequestPermissionRationale(it, Manifest.permission.CAMERA)) {
            Toast.makeText(it, getString(R.string.permission_camera), Toast.LENGTH_SHORT).show()
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCode)
        }
    }
}

@Suppress("unused")
fun Fragment.checkPermissionForCamera(): Boolean {
    return activity?.checkPermissionForCamera() ?: false
}

@Suppress("unused")
fun Activity.requestPermissionForLocation(requestCode: Int) {
    if (ActivityCompat.shouldShowRequestPermissionRationale(
            this, Manifest.permission.ACCESS_FINE_LOCATION
        ) || ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
    ) {
        Toast.makeText(this, getString(R.string.permission_location), Toast.LENGTH_SHORT).show()
    } else {

        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ), requestCode
        )
    }
}

@Suppress("unused")
fun Activity.checkPermissionForLocation(): Boolean {
    val fine =
        when (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            PackageManager.PERMISSION_GRANTED -> true
            else -> false
        }

    val coarse =
        when (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            PackageManager.PERMISSION_GRANTED -> true
            else -> false
        }
    return fine && coarse
}

@Suppress("unused")
fun Fragment.requestPermissionForLocation(requestCode: Int) {
    this.activity?.let {

        if (ActivityCompat.shouldShowRequestPermissionRationale(
                it, Manifest.permission.ACCESS_FINE_LOCATION
            ) || ActivityCompat.shouldShowRequestPermissionRationale(
                it,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        ) {
            Toast.makeText(it, getString(R.string.permission_location), Toast.LENGTH_SHORT).show()
        } else {

            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), requestCode
            )
        }

    }
}

@Suppress("unused")
fun Fragment.checkPermissionForLocation(): Boolean {
    return activity?.checkPermissionForLocation() ?: false
}