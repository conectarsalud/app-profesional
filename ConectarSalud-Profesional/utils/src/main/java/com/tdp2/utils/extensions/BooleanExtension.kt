package com.tdp2.utils.extensions

@Suppress("unused")
inline fun <reified T> Boolean?.ifTrue(statements: () -> T) {
    if (this == true)
        statements()
}

@Suppress("unused")
inline fun <reified T> Boolean?.ifFalse(statements: () -> T) {
    if (this == false)
        statements()
}

@Suppress("unused")
fun Boolean.toInt(): Int {
    return when (this) {
        true -> 1
        false -> 0
    }
}