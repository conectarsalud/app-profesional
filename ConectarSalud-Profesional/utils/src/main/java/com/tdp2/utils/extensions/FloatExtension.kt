package com.tdp2.utils.extensions

import android.content.res.Resources

@Suppress("unused")
val Float.dp: Float
    get() = (this / Resources.getSystem().displayMetrics.density)

@Suppress("unused")
val Float.px: Float
    get() = (this * Resources.getSystem().displayMetrics.density)