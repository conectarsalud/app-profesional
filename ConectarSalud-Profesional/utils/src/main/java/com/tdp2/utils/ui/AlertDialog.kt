package com.tdp2.utils.ui

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.CompoundButton
import androidx.annotation.StringRes
import com.tdp2.utils.R
import kotlinx.android.synthetic.main.dialog_sn_alert.*

class AlertDialog private constructor(context: Context) : Dialog(context, R.style.DialogTheme) {

    private var param = AlertParam()
    var isChecked = false

    init {
        val windowLayoutParams = WindowManager.LayoutParams()
        windowLayoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        windowLayoutParams.dimAmount = 0.3f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_sn_alert)
        initialize()
    }

    private fun initialize() {

        setCancelable(param.cancelable)
        this.setOnCancelListener(param.onCancelListener)
        this.setOnDismissListener(param.onDismissListener)

        if (param.title != null) {
            this.titleTextView.text = param.title
        }

        if (param.message != null) {
            this.messageTextView.text = param.message
        } else {
            this.messageTextView.visibility = View.GONE
            this.separatorLine.visibility = View.GONE
        }

        if (param.positiveButtonText != null) {
            this.positiveButton.visibility = View.VISIBLE
            this.positiveButton.text = param.positiveButtonText
        } else {
            this.positiveButton.visibility = View.GONE
        }

        if (param.positiveButtonListener != null) {
            this.positiveButton.setOnClickListener {
                param.positiveButtonListener?.onClick(this, DialogInterface.BUTTON_POSITIVE)
                this.dismiss()
            }
        }

        if (param.negativeButtonText != null) {
            this.negativeButton.visibility = View.VISIBLE
            this.negativeButton.text = param.negativeButtonText
        } else {
            this.negativeButton.visibility = View.GONE
        }

        if (param.negativeButtonListener != null) {
            this.negativeButton.setOnClickListener {
                param.negativeButtonListener?.onClick(this, DialogInterface.BUTTON_NEGATIVE)
                this.dismiss()
            }
        }

        if (negativeButton.visibility == View.VISIBLE && positiveButton.visibility == View.VISIBLE) {
            buttonMargin.visibility = View.VISIBLE
        } else {
            buttonMargin.visibility = View.GONE
        }

        if (param.showBackButton) {
            this.backButton.visibility = View.VISIBLE
            this.backButton.setOnClickListener {
                this.dismiss()
            }
        } else {
            this.backButton.visibility = View.INVISIBLE
        }

        if (param.checkboxText != null) {
            isChecked = checkbox.isChecked
            checkbox.visibility = View.VISIBLE
            checkbox.text = param.checkboxText
        } else {
            checkbox.visibility = View.GONE
        }

        if (param.checkBoxListener != null) {
            checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                this.isChecked = isChecked
                param.checkBoxListener?.onCheckedChanged(buttonView, isChecked)
            }

        }

        if (param.contentView != null) {
            scrollview.visibility = View.VISIBLE
            this.separatorLine.visibility = View.VISIBLE

            val lp = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
            )
            scrollview.addView(param.contentView, 0, lp)
        } else {
            scrollview.visibility = View.GONE
        }

    }

    private class AlertParam {
        var title: CharSequence? = null
        var message: CharSequence? = null
        var positiveButtonText: CharSequence? = null
        var negativeButtonText: CharSequence? = null
        var positiveButtonListener: DialogInterface.OnClickListener? = null
        var negativeButtonListener: DialogInterface.OnClickListener? = null
        var onCancelListener: DialogInterface.OnCancelListener? = null
        var onDismissListener: DialogInterface.OnDismissListener? = null
        var showBackButton: Boolean = true
        var cancelable: Boolean = true
        var checkBoxListener: CompoundButton.OnCheckedChangeListener? = null
        var checkboxText: CharSequence? = null
        var contentView: View? = null
    }

    @Suppress("unused")
    class Builder(private var context: Context) {
        private var param = AlertParam()

        fun setTitle(@StringRes textId: Int): Builder =
            apply { param.title = context.getText(textId) }

        fun setMessage(@StringRes textId: Int): Builder =
            apply { param.message = context.getText(textId) }

        fun setShowBackButton(showBackButton: Boolean): Builder =
            apply { param.showBackButton = showBackButton }

        fun setPositiveButton(@StringRes textId: Int, listener: DialogInterface.OnClickListener?): Builder {
            param.positiveButtonText = context.getText(textId)
            param.positiveButtonListener = listener
            return this
        }

        fun setNegativeButton(@StringRes textId: Int, listener: DialogInterface.OnClickListener?): Builder {
            param.negativeButtonText = context.getText(textId)
            param.negativeButtonListener = listener
            return this
        }

        fun setCheckbox(@StringRes textId: Int, listener: CompoundButton.OnCheckedChangeListener?): Builder {
            param.checkboxText = context.getText(textId)
            param.checkBoxListener = listener
            return this
        }

        fun setOnCancelListener(listener: DialogInterface.OnCancelListener?): Builder {
            param.onCancelListener = listener
            return this
        }

        fun setOnDismissListener(listener: DialogInterface.OnDismissListener?): Builder {
            param.onDismissListener = listener
            return this
        }

        fun setCancelable(cancelable: Boolean): Builder =
            apply { param.cancelable = cancelable }

        fun setContentView(v: View): Builder =
            apply { param.contentView = v }

        fun create(): AlertDialog {
            val dialog = AlertDialog(context)
            dialog.param = param
            return dialog
        }
    }
}