package com.tdp2.utils.extensions

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.TypedValue
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.tdp2.utils.R

val Context.networkState: NetworkInfo?
    get() =
        (this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo

@ColorInt
fun Context.themeAttributeToColor(@AttrRes themeAttributeId: Int, @ColorRes fallbackColorId: Int): Int {
    val outValue = TypedValue()
    return when (theme.resolveAttribute(themeAttributeId, outValue, true)) {
        true -> {
            when (outValue.resourceId) {
                0 -> outValue.data // Este es el color
                else -> ContextCompat.getColor(this, outValue.resourceId)
            }
        }
        false -> fallbackColorId
    }
}

@ColorInt
fun Context.themePrimaryColor(): Int {
    return themeAttributeToColor(R.attr.colorPrimary, R.color.colorRed)
}

@Suppress("unused")
@ColorInt
fun Context.themePrimaryDarkColor(): Int {
    return themeAttributeToColor(R.attr.colorPrimaryDark, R.color.colorRed)
}

@ColorInt
fun Context.themeAccentColor(): Int {
    return themeAttributeToColor(R.attr.colorAccent, R.color.colorRed)
}

@Suppress("unused")
@ColorInt
fun Context.themeSecondaryColor(): Int {
    return themeAttributeToColor(R.attr.colorSecondary, R.color.colorRed)
}
