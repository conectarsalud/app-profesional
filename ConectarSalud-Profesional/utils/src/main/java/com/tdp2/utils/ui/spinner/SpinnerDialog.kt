package com.tdp2.utils.ui.spinner

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.annotation.StringRes
import androidx.recyclerview.widget.LinearLayoutManager
import com.tdp2.utils.R
import kotlinx.android.synthetic.main.dialog_spinner_alert.*

class SpinnerDialog<T>(context: Context) : Dialog(context, R.style.DialogTheme) {

    private var param = SpinnerParam<T>()
    private var adapter: SpinnerDialogAdapter<T>? = null

    val selectedItems: List<T>
        get() {
            return adapter?.selectedItems ?: listOf()
        }

    private val onClickListener = fun(position: Int) {
        if (param.dismissAfterSelect) {
            dismiss()
        }
    }

    init {
        val windowLayoutParams = WindowManager.LayoutParams()
        windowLayoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        windowLayoutParams.dimAmount = 0.3f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_spinner_alert)
        initialize()
    }

    private fun initialize() {

        adapter = SpinnerDialogAdapter(
            param.list ?: listOf(),
            param.multipleSelection,
            param.maxSelection,
            param.style,
            onClickListener
        )
        adapter?.spinnerDataSource = param.spinnerDataSource

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = adapter

        setCancelable(param.cancelable)
        this.setOnCancelListener(param.onCancelListener)
        this.setOnDismissListener(param.onDismissListener)

        if (param.title != null) {
            this.titleTextView.text = param.title
        }

        if (param.positiveButtonText != null) {
            this.positiveButton.visibility = View.VISIBLE
            this.positiveButton.text = param.positiveButtonText
        } else {
            this.positiveButton.visibility = View.GONE
        }

        if (param.positiveButtonListener != null) {
            this.positiveButton.setOnClickListener {
                param.positiveButtonListener?.onClick(this, DialogInterface.BUTTON_POSITIVE)
                this.dismiss()
            }
        }

        if (param.negativeButtonText != null) {
            this.negativeButton.visibility = View.VISIBLE
            this.negativeButton.text = param.negativeButtonText
        } else {
            this.negativeButton.visibility = View.GONE
        }

        if (param.negativeButtonListener != null) {
            this.negativeButton.setOnClickListener {
                param.negativeButtonListener?.onClick(this, DialogInterface.BUTTON_NEGATIVE)
                this.dismiss()
            }
        }

        if (negativeButton.visibility == View.VISIBLE && positiveButton.visibility == View.VISIBLE) {
            buttonMargin.visibility = View.VISIBLE
        } else if (negativeButton.visibility == View.GONE && positiveButton.visibility == View.GONE) {
            buttonMargin.visibility = View.GONE
            buttons.visibility = View.GONE
        } else {
            buttonMargin.visibility = View.GONE
        }

        if (param.showBackButton) {
            this.backButton.visibility = View.VISIBLE
            this.backButton.setOnClickListener {
                this.dismiss()
            }
        } else {
            this.backButton.visibility = View.INVISIBLE
        }

    }

    private class SpinnerParam<T> {
        var title: CharSequence? = null
        var list: List<Triple<String, Boolean, T>>? = null
        var positiveButtonText: CharSequence? = null
        var negativeButtonText: CharSequence? = null
        var positiveButtonListener: DialogInterface.OnClickListener? = null
        var negativeButtonListener: DialogInterface.OnClickListener? = null
        var onCancelListener: DialogInterface.OnCancelListener? = null
        var onDismissListener: DialogInterface.OnDismissListener? = null
        var showBackButton: Boolean = true
        var cancelable: Boolean = true
        var multipleSelection: Boolean = false
        var dismissAfterSelect: Boolean = false
        var style: SpinnerStyle = SpinnerStyle.DEFAULT
        var maxSelection: Int? = null
        var spinnerDataSource: SpinnerDataSource? = null
    }

    @Suppress("unused")
    class Builder<T>(private var context: Context) {
        private var param =
            SpinnerParam<T>()

        fun setSpinnerDataSource(spinnerDataSource: SpinnerDataSource): Builder<T> =
            apply { param.spinnerDataSource = spinnerDataSource }

        fun setTitle(@StringRes textId: Int): Builder<T> =
            apply { param.title = context.getText(textId) }

        fun setShowBackButton(showBackButton: Boolean): Builder<T> =
            apply { param.showBackButton = showBackButton }

        fun setPositiveButton(@StringRes textId: Int, listener: DialogInterface.OnClickListener?): Builder<T> {
            param.positiveButtonText = context.getText(textId)
            param.positiveButtonListener = listener
            return this
        }

        fun setNegativeButton(@StringRes textId: Int, listener: DialogInterface.OnClickListener?): Builder<T> {
            param.negativeButtonText = context.getText(textId)
            param.negativeButtonListener = listener
            return this
        }

        fun setOnCancelListener(listener: DialogInterface.OnCancelListener?): Builder<T> {
            param.onCancelListener = listener
            return this
        }

        fun setOnDismissListener(listener: DialogInterface.OnDismissListener?): Builder<T> {
            param.onDismissListener = listener
            return this
        }

        fun setCancelable(cancelable: Boolean): Builder<T> =
            apply { param.cancelable = cancelable }

        fun setStyle(style: SpinnerStyle): Builder<T> =
            apply { param.style = style }

        fun setDismissAfterSelect(dismissAfterSelect: Boolean): Builder<T> =
            apply { param.dismissAfterSelect = dismissAfterSelect }

        fun multipleSelection(
            list: List<Triple<String, Boolean, T>>,
            max: Int? = null
        ): Builder<T> =
            apply {
                param.list = list
                param.multipleSelection = true
                param.maxSelection = max
            }

        fun singleSelection(list: List<Triple<String, Boolean, T>>): Builder<T> =
            apply {
                param.list = list
                param.multipleSelection = false
            }

        fun create(): SpinnerDialog<T> {
            val dialog = SpinnerDialog<T>(context)
            dialog.param = param
            return dialog
        }
    }
}