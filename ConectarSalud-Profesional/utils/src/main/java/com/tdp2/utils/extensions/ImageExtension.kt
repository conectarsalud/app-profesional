package com.tdp2.utils.extensions

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import androidx.exifinterface.media.ExifInterface
import java.io.*

fun File.cameraPhotoOrientation(): Int {
    val rotate = 0

    try {
        val exifInterface = ExifInterface(absolutePath)


        when (exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)) {
            ExifInterface.ORIENTATION_ROTATE_270 -> return 270
            ExifInterface.ORIENTATION_ROTATE_180 -> return 180
            ExifInterface.ORIENTATION_ROTATE_90 -> return 90
        }
    } catch (e: IOException) {
    }
    return rotate
}

fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
    // Raw height and width of image
    val (height: Int, width: Int) = options.run { outHeight to outWidth }
    var inSampleSize = 1

    if (height > reqHeight || width > reqWidth) {

        val halfHeight: Int = height / 2
        val halfWidth: Int = width / 2

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
            inSampleSize *= 2
        }
    }

    return inSampleSize
}

fun File.decodeBitmap(reqWidth: Int, reqHeight: Int): Bitmap {
    // First decode with inJustDecodeBounds=true to check dimensions
    return BitmapFactory.Options().run {
        inJustDecodeBounds = true
        BitmapFactory.decodeFile(absolutePath, this)

        // Calculate inSampleSize
        inSampleSize = calculateInSampleSize(this, reqWidth, reqHeight)

        // Decode bitmap with inSampleSize set
        inJustDecodeBounds = false

        BitmapFactory.decodeFile(absolutePath, this)
    }
}

fun File.compressImage(rotateDegrees: Int, reqWidth: Int, reqHeight: Int, quality: Int) {

    var bmp = decodeBitmap(reqWidth, reqHeight)

    val matrix = Matrix()
    matrix.postRotate(rotateDegrees.toFloat())
    bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.width, bmp.height, matrix, true)

    val outputStream: FileOutputStream
    try {
        outputStream = FileOutputStream(this)
        bmp.compress(Bitmap.CompressFormat.JPEG, quality, outputStream)
        outputStream.flush()
        outputStream.close()
        bmp.recycle()
    } catch (e: FileNotFoundException) {
    } catch (e: IOException) {
    }
}


fun Uri.createTempFile(context: Context): File? {
    val file: File?
    val fileName = "temp_image"
    try {
        file = File.createTempFile(fileName, ".jpg", context.cacheDir)
    } catch (e: IOException) {
        return null
    }

    var bis: BufferedInputStream? = null
    var bos: BufferedOutputStream? = null
    try {
        val imageStream = context.contentResolver.openInputStream(this)
        bis = BufferedInputStream(imageStream!!)
        bos = BufferedOutputStream(FileOutputStream(file!!.absolutePath, false))
        val buf = ByteArray(1024)
        bis.read(buf)
        do {
            bos.write(buf)
        } while (bis.read(buf) != -1)

    } catch (e: IOException) {

    } finally {
        try {
            bis?.close()
            bos?.close()
        } catch (e: IOException) {

        }
    }

    return file
}