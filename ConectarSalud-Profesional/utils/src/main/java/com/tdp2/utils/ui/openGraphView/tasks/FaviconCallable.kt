package com.tdp2.utils.ui.openGraphView.tasks

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.TextUtils
import java.io.IOException
import java.net.URL
import java.util.concurrent.Callable

class FaviconCallable(private val url: String) : Callable<Bitmap> {

    private val BASE_URL = "http://www.google.com/s2/favicons?domain="

    override fun call(): Bitmap? {
        if (TextUtils.isEmpty(url)) {
            return null
        }
        return try {
            val inputStream =
                URL(BASE_URL + url).openStream()
            BitmapFactory.decodeStream(inputStream)
        } catch (e: IOException) {
            return null
        }
    }
}