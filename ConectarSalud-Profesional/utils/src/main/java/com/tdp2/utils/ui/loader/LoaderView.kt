package com.tdp2.utils.ui.loader

import android.animation.ObjectAnimator
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageView
import android.widget.RelativeLayout
import com.tdp2.utils.R

@Suppress("unused")
class LoaderView : RelativeLayout {

    private var mRootContainer: View
    private var mIconImageView: ImageView
    private var mDrawable: Drawable? = null
    private var mTintColor: ColorStateList? = null
    private var animation: ObjectAnimator? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

        mRootContainer = inflate(context, R.layout.view_loader, this)
        mIconImageView = mRootContainer.findViewById(R.id.icon)

        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.ViewLoader, 0, 0
        ).apply {

            try {

                if (hasValue(R.styleable.ViewLoader_iconTint)) {
                    mTintColor = ColorStateList.valueOf(R.styleable.ViewLoader_iconTint)
                }

                if (hasValue(R.styleable.ViewLoader_icon)) {
                    mDrawable = getDrawable(R.styleable.ViewLoader_icon)

                    mDrawable?.apply {
                        mIconImageView.setImageDrawable(this)
                        mTintColor?.apply {
                            mIconImageView.imageTintList = this
                            mIconImageView.imageTintMode = PorterDuff.Mode.ADD
                        }
                    }
                }

            } finally {
                recycle()
            }
        }

        this.visibility = View.GONE
    }

    fun show() {
        animation = ObjectAnimator.ofFloat(
            mIconImageView,
            "rotationY", 0.0f, 360f
        ).apply {
            duration = 600
            repeatCount = ObjectAnimator.INFINITE
            interpolator = AccelerateDecelerateInterpolator()
            start()
        }
        this.visibility = View.VISIBLE
    }

    fun hide() {
        this.visibility = View.GONE
        animation?.cancel()
    }

}