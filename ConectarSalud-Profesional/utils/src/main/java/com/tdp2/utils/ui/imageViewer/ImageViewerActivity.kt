package com.tdp2.utils.ui.imageViewer

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tdp2.utils.R
import com.facebook.common.util.UriUtil
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.drawable.ProgressBarDrawable
import com.facebook.drawee.drawable.ScalingUtils
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder
import com.facebook.drawee.interfaces.DraweeController
import kotlinx.android.synthetic.main.activity_image_viewer.*


class ImageViewerActivity : AppCompatActivity() {

    companion object {

        @JvmStatic
        fun newIntent(ctx: Context?, url: String?): Intent? {
            val data = Uri.parse(url)
            return Intent(ctx, ImageViewerActivity::class.java).setData(data)
        }

        @Suppress("unused")
        @JvmStatic
        fun newIntent(ctx: Context?, resource: Int): Intent? {
            val uri = Uri.Builder()
                .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                .path(resource.toString())
                .build()
            return Intent(ctx, ImageViewerActivity::class.java).setData(uri)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val data = intent.data ?: throw IllegalArgumentException("No data to display")
        Fresco.initialize(this)
        setContentView(R.layout.activity_image_viewer)


        val ctrl: DraweeController = Fresco.newDraweeControllerBuilder().setUri(
            data
        ).setTapToRetryEnabled(true).build()
        val hierarchy = GenericDraweeHierarchyBuilder(resources)
            .setActualImageScaleType(ScalingUtils.ScaleType.FIT_CENTER)
            .setProgressBarImage(ProgressBarDrawable())
            //.setPlaceholderImage(ContextCompat.getDrawable(this,R.drawable.ic_image_placeholder))
            .build()

        zoomableImageView?.apply {
            controller = ctrl
            setHierarchy(hierarchy)
        }

    }
}
