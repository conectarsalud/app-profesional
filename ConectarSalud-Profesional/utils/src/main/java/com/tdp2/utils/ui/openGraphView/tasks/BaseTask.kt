package com.tdp2.utils.ui.openGraphView.tasks

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Callable
import java.util.concurrent.FutureTask

abstract class BaseTask<T>(callable: Callable<T>, private val listener: OnLoadListener<T>) : FutureTask<T>(callable) {

    private val handler = Handler(Looper.getMainLooper())

    interface OnLoadListener<T> {
        fun onLoadSuccess(t: T)
        fun onLoadError(e: Throwable?)
    }

    fun onSuccess(t: T) {
        handler.post { listener.onLoadSuccess(t) }
    }

    fun onError(e: Throwable?) {
        handler.post { listener.onLoadError(e) }
    }
}