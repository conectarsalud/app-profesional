package com.tdp2.conectarsaludprofesional.data.remote.utils

import com.tdp2.conectarsaludprofesional.BuildConfig
import okhttp3.Protocol
import okhttp3.Request

fun okhttp3.Response.Builder.buildSuccess(): okhttp3.Response {
    val mockRequest = Request.Builder().url(BuildConfig.HOST).build()
    request(mockRequest)
    code(200)
    message("OK!")
    protocol(Protocol.HTTP_1_1)
    return build()
}