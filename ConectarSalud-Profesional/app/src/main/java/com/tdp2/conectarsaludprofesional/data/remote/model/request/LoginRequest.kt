package com.tdp2.conectarsaludprofesional.data.remote.model.request

class LoginRequest (val username: String, val password: String)