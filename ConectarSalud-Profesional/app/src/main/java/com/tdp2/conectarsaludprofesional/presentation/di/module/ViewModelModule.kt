package com.tdp2.conectarsaludprofesional.presentation.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tdp2.conectarsaludprofesional.presentation.di.base.ViewModelKey
import com.tdp2.conectarsaludprofesional.presentation.ui.access.LoginViewModel
import com.tdp2.conectarsaludprofesional.presentation.ui.main.HomeViewModel
import com.tdp2.conectarsaludprofesional.presentation.ui.videocall.VideoCallViewModel
import com.tdp2.conectarsaludprofesional.utils.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    //@Bind es simil a provider, devuelve la interfaz y recibe una implementación de la misma (esto obliga a que la clase sea abstracta).

    //Injecta este objeto en un Map usando ViewModelKey como key y el Provider como value.
    // El provider va a crear el objeto

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VideoCallViewModel::class)
    abstract fun bindVideoCallViewModel(videoCallViewModel: VideoCallViewModel): ViewModel

}