package com.tdp2.conectarsaludprofesional.presentation.di.module.builder

import com.tdp2.conectarsaludprofesional.presentation.ui.MainActivity
import com.tdp2.conectarsaludprofesional.presentation.ui.access.LoginActivity
import com.tdp2.conectarsaludprofesional.presentation.ui.main.HomeActivity
import com.tdp2.conectarsaludprofesional.presentation.ui.scanner.ScannerActivity
import com.tdp2.conectarsaludprofesional.presentation.ui.videocall.VideoCallActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityBuilderModule {

    //Generates an AndroidInjector for the return type of this method
    // Crea un Injector para el tipo que se retorna, lo que va a permitir luego inyectar
    // Las dependencias a la activity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeHomeActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun contributeVideoCallActivity(): VideoCallActivity

}