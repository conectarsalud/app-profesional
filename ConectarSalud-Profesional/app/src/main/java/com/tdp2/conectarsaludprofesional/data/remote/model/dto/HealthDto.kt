package com.tdp2.conectarsaludprofesional.data.remote.model.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginDto(
    @Json(name = "token") val token: TokenDto
)

@JsonClass(generateAdapter = true)
data class TokenDto(
    @Json(name = "token") val authToken: String,
    @Json(name = "tokenExpiration") val tokenExpiration: Long
)

@JsonClass(generateAdapter = true)
data class NewAppointmentDto(
    @Json(name = "appointment") val appointment: AppointmentDto
)

@JsonClass(generateAdapter = true)
data class AppointmentDto(
    @Json(name = "id") val id: Int,
    @Json(name = "estado") val state: String,
    @Json(name = "channel_id") val channelId: String?
)