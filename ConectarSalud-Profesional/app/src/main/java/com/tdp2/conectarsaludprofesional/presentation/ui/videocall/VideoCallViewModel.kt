package com.tdp2.conectarsaludprofesional.presentation.ui.videocall

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.tdp2.conectarsaludprofesional.R
import com.tdp2.conectarsaludprofesional.data.local.model.Appointment
import com.tdp2.conectarsaludprofesional.data.mapper.toAppointment
import com.tdp2.conectarsaludprofesional.data.remote.utils.NetworkConnectionException
import com.tdp2.conectarsaludprofesional.data.remote.utils.Result
import com.tdp2.conectarsaludprofesional.data.repository.HealthRepository
import com.tdp2.conectarsaludprofesional.presentation.ui.base.BaseViewModel
import com.tdp2.conectarsaludprofesional.utils.Constants
import kotlinx.coroutines.launch
import javax.inject.Inject

class VideoCallViewModel
@Inject constructor(private val healthRepository: HealthRepository) : BaseViewModel() {

    private val TAG: String = "VIDEO_CALL_VM"

    private val mFinishAppointment = MutableLiveData<Appointment>()
    val finishAppointment: LiveData<Appointment>
        get() = mFinishAppointment
    private val mCancelAppointment = MutableLiveData<Appointment>()
    val cancelAppointment: LiveData<Appointment>
        get() = mCancelAppointment
    private val mCancel = MutableLiveData<Boolean>()
    val cancel: LiveData<Boolean>
        get() = mCancel

    fun makeSnackBar(msg: String) {
        showSnackbarMessage(msg)
    }

    fun makeToast(msg: String) {
        showToastMessage(msg)
    }

    fun finishAppointment(appointmentId: String) {

        mDataLoading.value = true
        viewModelScope.launch {
            when (val result = healthRepository.finishAppointment(appointmentId)) {
                // Successful HTTP result
                is Result.Ok -> {
                    mFinishAppointment.value = result.value.appointment.toAppointment()
                }
                // Any HTTP error
                is Result.Error -> {
                    when (result.exception.code()) {
                        Constants.StatusCode.INVALID_APPOINTMENT_ACTION -> mCancel.value = true
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }

    fun cancelAppointment(appointmentId: String) {

        mDataLoading.value = true
        viewModelScope.launch {
            when (val result = healthRepository.cancelAppointment(appointmentId)) {
                // Successful HTTP result
                is Result.Ok -> {
                    mCancelAppointment.value = result.value.appointment.toAppointment()
                }
                // Any HTTP error
                is Result.Error -> {
                    when (result.exception.code()) {
                        Constants.StatusCode.INVALID_APPOINTMENT_ACTION -> showWarningDialog(R.string.invalid_appointment_cancel)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }
}