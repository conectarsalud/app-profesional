package com.tdp2.conectarsaludprofesional.presentation.ui.access

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.tdp2.conectarsaludprofesional.R
import com.tdp2.conectarsaludprofesional.data.local.model.FieldState
import com.tdp2.conectarsaludprofesional.data.remote.utils.NetworkConnectionException
import com.tdp2.conectarsaludprofesional.data.repository.HealthRepository
import com.tdp2.conectarsaludprofesional.presentation.ui.base.BaseViewModel
import com.tdp2.conectarsaludprofesional.utils.Constants
import com.tdp2.conectarsaludprofesional.data.remote.utils.Result
import com.tdp2.utils.extensions.isCredentialValid
import com.tdp2.utils.general.Event
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel
@Inject constructor(private val healthRepository: HealthRepository) : BaseViewModel() {

    private val TAG: String = "LOGIN_VM"

    private val mLoginCredentialFormState = MutableLiveData<FieldState>()
    val loginCredentialFormState: LiveData<FieldState> = mLoginCredentialFormState

    private val mLoginPasswordFormState = MutableLiveData<FieldState>()
    val loginPasswordFormState: LiveData<FieldState> = mLoginPasswordFormState

    private val mLoginButtonFormState = MutableLiveData<Boolean>()
    val loginButtonFormState: LiveData<Boolean> = mLoginButtonFormState

    private val mLoginSuccess = MutableLiveData<Boolean>()
    val loginSuccess: LiveData<Boolean> = mLoginSuccess

    private val mUserLoggedState = MutableLiveData<Event<Boolean>>()
    val userLoggedState: LiveData<Event<Boolean>> = mUserLoggedState

    fun makeSnackBar(msg: String) {
        showSnackbarMessage(msg)
    }

    fun makeToast(msg: String) {
        showToastMessage(msg)
    }

    fun initialize() {

        if (healthRepository.isUserLogged()) {
            mUserLoggedState.value = Event(true)

        } else {
            healthRepository.logout()
            mUserLoggedState.value = Event(false)
        }
    }

    fun validateCredential(credential: String) {
        val state = FieldState()

        if (credential.isEmpty()) {
            state.isValid = false
            state.error = R.string.login_empty_credential
        } else if (!credential.isCredentialValid()) {
            state.isValid = false
            state.error = R.string.login_invalid_credential
        } else {
            state.isValid = true
            state.error = null
        }
        mLoginCredentialFormState.value = state
    }

    fun validatePassword(password: String) {
        val state = FieldState()

        if (password.isEmpty()) {
            state.isValid = false
            state.error = R.string.login_empty_password
        } else {
            state.isValid = true
            state.error = null
        }
        mLoginPasswordFormState.value = state
    }

    fun updateButtonState(credential: String, password: String) {
        mLoginButtonFormState.value = credential.isCredentialValid() && password.isNotEmpty()
    }

    fun login(credential: String, password: String) {
        validateCredential(credential)
        validatePassword(password)
        if (mLoginCredentialFormState.value?.isValid == true && mLoginPasswordFormState.value?.isValid == true) {
            doLogin(credential, password)
        }
    }

    private fun doLogin(credential: String, password: String) {

        mDataLoading.value = true
        viewModelScope.launch {
            when (val result = healthRepository.login(credential, password)) {
                // Successful HTTP result
                is Result.Ok -> {
                    mLoginSuccess.value = true
                }
                // Any HTTP error
                is Result.Error -> {
                    when (result.exception.code()) {
                        Constants.StatusCode.UNAUTHORIZED -> showErrorDialog(R.string.login_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }
}