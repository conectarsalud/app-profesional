package com.tdp2.conectarsaludprofesional.presentation.di.module

import android.app.Application
import android.content.Context
import com.tdp2.conectarsaludprofesional.presentation.di.base.ApplicationContext
import com.tdp2.conectarsaludprofesional.utils.AppApplication
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

    //Importante: agregar la anotation @ApplicationContext donde se inyecta
    @Provides
    @ApplicationContext
    internal fun provideContext(appApplication: AppApplication): Context {
        return appApplication.applicationContext
    }

    @Provides
    internal fun provideApplication(appApplication: AppApplication): Application {
        return appApplication
    }
}