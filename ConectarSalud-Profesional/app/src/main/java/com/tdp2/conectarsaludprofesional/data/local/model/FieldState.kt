package com.tdp2.conectarsaludprofesional.data.local.model

import androidx.annotation.StringRes

class FieldState(
    @StringRes var error: Int? = null,
    var isValid: Boolean = false
)