package com.tdp2.conectarsaludprofesional.data.remote

import com.tdp2.conectarsaludprofesional.data.remote.model.dto.HospitalsDataDto
import com.tdp2.conectarsaludprofesional.data.remote.model.dto.LoginDto
import com.tdp2.conectarsaludprofesional.data.remote.model.dto.NewAppointmentDto
import com.tdp2.conectarsaludprofesional.data.remote.model.request.LoginRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiClient {

    @GET("/api/hospitals/all")
    fun getHospitalsData(): Call<HospitalsDataDto>

    @POST("doctor/login")
    fun login(@Body body: LoginRequest): Call<LoginDto>

    @POST("doctor/appointment/{appointmentId}/start")
    fun startAppointment(@Path("appointmentId") id: String): Call<NewAppointmentDto>

    @POST("doctor/appointment/{appointmentId}/finish")
    fun finishAppointment(@Path("appointmentId") id: String): Call<NewAppointmentDto>

    @POST("doctor/appointment/{appointmentId}/cancel")
    fun cancelAppointment(@Path("appointmentId") id: String): Call<NewAppointmentDto>
}