package com.tdp2.conectarsaludprofesional.presentation.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.zxing.integration.android.IntentIntegrator
import com.tdp2.conectarsaludprofesional.R
import com.tdp2.conectarsaludprofesional.data.local.model.Appointment
import com.tdp2.conectarsaludprofesional.data.local.model.ScannedAppointment
import com.tdp2.conectarsaludprofesional.presentation.ui.access.LoginActivity
import com.tdp2.conectarsaludprofesional.presentation.ui.base.BaseActivity
import com.tdp2.conectarsaludprofesional.presentation.ui.scanner.ScannerActivity
import com.tdp2.conectarsaludprofesional.presentation.ui.videocall.VideoCallActivity
import com.tdp2.utils.extensions.*
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class HomeActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, HomeActivity::class.java)
        }

        private const val TAG = "HOME_ACT"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var homeViewModel: HomeViewModel
    private var scannedAppointment: ScannedAppointment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        homeViewModel = ViewModelProvider(this, viewModelFactory).get(HomeViewModel::class.java)

        setupToolbar(toolbar, R.string.app_name)
        setSupportActionBar(toolbar)

        setupListeners()
        setupObservers()
    }

    private fun setupListeners() {

        scanButton.setOnClickListener {
            IntentIntegrator(this).apply {
                captureActivity = ScannerActivity::class.java
                setPrompt(resources.getString(R.string.scanner))
                setBeepEnabled(false)
                initiateScan()
            }
        }
    }

    private fun setupObservers() {

        homeViewModel.dataLoading.observe(this, Observer { loading ->
            when (loading) {
                true -> loadingContentView.visible(true)
                false -> loadingContentView.gone(true)
            }
        })

        homeViewModel.startAppointment.observe(this, Observer { app ->
            scannedAppointment?.let {
                val appointment =
                    Appointment(it.appointment_id.toString(), app.state, it.channel_id)
                val intent = VideoCallActivity.newIntent(this, appointment)
                startActivity(intent)
            }
        })

        mainView.setupSnackbar(this, homeViewModel.snackBarMessage, Snackbar.LENGTH_LONG)
        mainView.setupToast(this, homeViewModel.toastMessage, Toast.LENGTH_LONG)
        mainView.setupErrorDialog(this, homeViewModel.errorDialog)
        mainView.setupWarningDialog(this, homeViewModel.warningDialog)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (result != null) {
            if (result.contents.isNullOrEmpty()) {
                Toast.makeText(this, R.string.home_scan_result, Toast.LENGTH_LONG).show()
            } else {
                scannedAppointment =
                    Gson().fromJson(result.contents, ScannedAppointment::class.java)
                scannedAppointment?.let {
                    homeViewModel.startAppointment(it.appointment_id.toString())
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu?.apply { menuInflater.inflate(R.menu.home_menu, this) }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.logout -> {
                homeViewModel.logout()
                val intent = LoginActivity.newIntent(this)
                startActivity(intent)
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
