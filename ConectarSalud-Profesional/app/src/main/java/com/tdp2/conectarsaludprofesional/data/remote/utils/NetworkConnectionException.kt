package com.tdp2.conectarsaludprofesional.data.remote.utils

import java.lang.Exception

class NetworkConnectionException: Exception("Network Connection Error")