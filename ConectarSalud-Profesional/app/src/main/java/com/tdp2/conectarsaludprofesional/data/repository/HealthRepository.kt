package com.tdp2.conectarsaludprofesional.data.repository

import android.content.Context
import com.tdp2.conectarsaludprofesional.R
import com.tdp2.conectarsaludprofesional.data.local.AppPreferences
import com.tdp2.conectarsaludprofesional.data.remote.ApiClient
import com.tdp2.conectarsaludprofesional.data.remote.model.dto.HospitalsDataDto
import com.tdp2.conectarsaludprofesional.data.remote.model.dto.LoginDto
import com.tdp2.conectarsaludprofesional.data.remote.model.dto.NewAppointmentDto
import com.tdp2.conectarsaludprofesional.data.remote.model.request.LoginRequest
import com.tdp2.conectarsaludprofesional.data.remote.utils.*
import com.tdp2.conectarsaludprofesional.presentation.di.base.ApplicationContext
import okhttp3.Response
import java.util.*
import javax.inject.Inject

class HealthRepository @Inject constructor(
    private val networkHandler: NetworkHandler,
    private val services: ApiClient,
    @ApplicationContext val context: Context
) {

    private var hospitalsDataDto: HospitalsDataDto? = null
    private var lastUpdate = 0L
    private val expiredHospitalsData: Long = 1000L * 60 * 60 // 60 min

    suspend fun getHospitalsData(): Result<HospitalsDataDto> {

        if (Date().time - lastUpdate > expiredHospitalsData) {
            hospitalsDataDto = null
        }
        hospitalsDataDto?.apply {
            return Result.Ok(this, Response.Builder().buildSuccess())
        }

        return when (networkHandler.isConnected) {
            true -> {
                val result = services.getHospitalsData().awaitResult()
                when (result) {
                    is Result.Ok -> {
                        hospitalsDataDto = result.value
                        lastUpdate = Date().time
                    }
                }
                result
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    fun isUserLogged(): Boolean {
        return AppPreferences.hasSession()
    }

    fun logout() {
        AppPreferences.clearSession()
    }

    suspend fun login(username: String, password: String): Result<LoginDto> {
        val request = LoginRequest(username, password)
        return when (networkHandler.isConnected) {
            true -> {
                val result = services.login(request).awaitResult()
                when (result) {
                    is Result.Ok -> {
                        context.resources
                        AppPreferences.authToken =
                            "${context.resources.getString(R.string.token)} ${result.value.token.authToken}"
                        val timestamp = result.value.token.tokenExpiration
                        if (timestamp != AppPreferences.timestamp) {
                            AppPreferences.timestamp = timestamp
                            AppPreferences.lastUpdate = Date().time
                        }
                    }
                }
                result
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun startAppointment(appointmentId: String): Result<NewAppointmentDto> {
        return when (networkHandler.isConnected) {
            true -> {
                services.startAppointment(appointmentId).awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun finishAppointment(appointmentId: String): Result<NewAppointmentDto> {
        return when (networkHandler.isConnected) {
            true -> {
                services.finishAppointment(appointmentId).awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }

    suspend fun cancelAppointment(appointmentId: String): Result<NewAppointmentDto> {
        return when (networkHandler.isConnected) {
            true -> {
                services.cancelAppointment(appointmentId).awaitResult()
            }
            false, null -> Result.Exception(NetworkConnectionException())
        }
    }
}