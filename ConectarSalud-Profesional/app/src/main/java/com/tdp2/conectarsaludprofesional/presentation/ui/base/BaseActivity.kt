package com.tdp2.conectarsaludprofesional.presentation.ui.base

import androidx.appcompat.widget.Toolbar
import com.tdp2.conectarsaludprofesional.R
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity() {

    fun setupToolbar(toolbar: Toolbar, titleId: Int) {
        toolbar.title = resources.getString(titleId)
    }

    fun setupToolbar(toolbar: Toolbar, title: String) {
        toolbar.title = title
    }

    fun addBackButton(toolbar: Toolbar) {
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        finish()
    }

}