package com.tdp2.conectarsaludprofesional.presentation.ui.scanner

import com.journeyapps.barcodescanner.CaptureActivity
import com.journeyapps.barcodescanner.DecoratedBarcodeView
import com.tdp2.conectarsaludprofesional.R
import kotlinx.android.synthetic.main.activity_scanner.*

class ScannerActivity : CaptureActivity() {

    override fun initializeContent(): DecoratedBarcodeView {
        setContentView(R.layout.activity_scanner)

        return qrScanner as DecoratedBarcodeView
    }
}
