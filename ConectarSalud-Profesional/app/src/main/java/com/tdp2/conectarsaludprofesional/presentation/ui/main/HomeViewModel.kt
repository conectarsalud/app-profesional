package com.tdp2.conectarsaludprofesional.presentation.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.tdp2.conectarsaludprofesional.R
import com.tdp2.conectarsaludprofesional.data.local.model.Appointment
import com.tdp2.conectarsaludprofesional.data.mapper.toAppointment
import com.tdp2.conectarsaludprofesional.data.remote.utils.NetworkConnectionException
import com.tdp2.conectarsaludprofesional.data.remote.utils.Result
import com.tdp2.conectarsaludprofesional.data.repository.HealthRepository
import com.tdp2.conectarsaludprofesional.presentation.ui.base.BaseViewModel
import com.tdp2.conectarsaludprofesional.utils.Constants
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel
@Inject constructor(private val healthRepository: HealthRepository) : BaseViewModel() {

    private val TAG: String = "HOME_VM"

    private val mStartAppointment = MutableLiveData<Appointment>()
    val startAppointment: LiveData<Appointment>
        get() = mStartAppointment

    fun makeSnackBar(msg: String) {
        showSnackbarMessage(msg)
    }

    fun makeToast(msg: String) {
        showToastMessage(msg)
    }

    fun logout() {
        healthRepository.logout()
    }

    fun startAppointment(appointmentId: String) {

        mDataLoading.value = true
        viewModelScope.launch {
            when (val result = healthRepository.startAppointment(appointmentId)) {
                // Successful HTTP result
                is Result.Ok -> {
                    mStartAppointment.value = result.value.appointment.toAppointment()
                }
                // Any HTTP error
                is Result.Error -> {
                    when (result.exception.code()) {
                        Constants.StatusCode.INVALID_APPOINTMENT_ACTION -> showErrorDialog(R.string.fail_appointment)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
                // Exception while request invocation
                is Result.Exception -> {
                    when (result.exception) {
                        is NetworkConnectionException -> showErrorDialog(R.string.connection_error)
                        else -> showErrorDialog(R.string.loading_error)
                    }
                }
            }
            mDataLoading.value = false
        }
    }
}