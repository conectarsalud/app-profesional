package com.tdp2.conectarsaludprofesional.presentation.ui.access

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.tdp2.conectarsaludprofesional.R
import com.tdp2.conectarsaludprofesional.presentation.ui.base.BaseActivity
import com.tdp2.conectarsaludprofesional.presentation.ui.main.HomeActivity
import com.tdp2.utils.extensions.*
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }

        private const val TAG = "LOGIN_ACT"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        loginViewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)

        setupListeners()
        setupObservers()

        loginViewModel.initialize()
    }

    private fun gotoHome() {
        val intent = HomeActivity.newIntent(this)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

    private fun setupListeners() {

        credentialEditText.afterTextChanged = {
            loginViewModel.updateButtonState(credentialEditText.text, passwordEditText.text)
        }

        credentialEditText.afterFocusChanged = {
            it.ifFalse {
                loginViewModel.validateCredential(credentialEditText.text)
            }
        }

        passwordEditText.afterTextChanged = {
            loginViewModel.updateButtonState(credentialEditText.text, passwordEditText.text)
        }

        passwordEditText.afterFocusChanged = {
            it.ifFalse { loginViewModel.validatePassword(passwordEditText.text) }
        }

        passwordEditText.setOnEditorActionListener { actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    hideKeyboard()
                    loginViewModel.validatePassword(passwordEditText.text)
                }
            }
            false
        }

        loginButton.setOnClickListener {
            loginViewModel.login(credentialEditText.text, passwordEditText.text)
        }
    }

    private fun setupObservers() {

        loginViewModel.userLoggedState.observe(this@LoginActivity, Observer { event ->
            event.getContentIfNotHandled()?.apply {
                if (this) gotoHome()
            }
        })

        loginViewModel.loginCredentialFormState.observe(this@LoginActivity, Observer {
            credentialEditText.error = it.error?.let {
                getString(it)
            }

            credentialEditText.setEndDrawableVisibility(it.isValid)
        })

        loginViewModel.loginPasswordFormState.observe(this@LoginActivity, Observer {
            passwordEditText.error = it.error?.let {
                getString(it)
            }
        })

        loginViewModel.loginButtonFormState.observe(this@LoginActivity, Observer {
            loginButton.isEnabled = it
        })

        loginViewModel.loginSuccess.observe(this@LoginActivity, Observer {
            if (it) {
                gotoHome()
            }
        })

        loginViewModel.dataLoading.observe(this, Observer { loading ->
            when (loading) {
                true -> loadingContentView.visible(true)
                false -> loadingContentView.gone(true)
            }
        })

        mainView.setupSnackbar(this, loginViewModel.snackBarMessage, Snackbar.LENGTH_LONG)
        mainView.setupToast(this, loginViewModel.toastMessage, Toast.LENGTH_LONG)
        mainView.setupErrorDialog(this, loginViewModel.errorDialog)
    }
}