package com.tdp2.conectarsaludprofesional.data.mapper

import com.tdp2.conectarsaludprofesional.data.local.model.Appointment
import com.tdp2.conectarsaludprofesional.data.local.model.Hospital
import com.tdp2.conectarsaludprofesional.data.remote.model.dto.AppointmentDto
import com.tdp2.conectarsaludprofesional.data.remote.model.dto.HospitalDto

fun HospitalDto.toHospital(): Hospital {

    return Hospital(
        id = this.id,
        name = this.name,
        address = this.address,
        phoneNumber = this.phoneNumber,
        guardPhoneNumber = this.guardPhoneNumber,
        specialty = this.specialty,
        neighborhood = this.neighborhood,
        district = this.district,
        lat = this.lat.toDouble(),
        long = this.long.toDouble()
    )
}

fun AppointmentDto.toAppointment(): Appointment {

    return Appointment(
        id = this.id.toString(),
        state = this.state,
        channelId = this.channelId
    )
}